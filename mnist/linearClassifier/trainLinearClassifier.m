function [weights] = trainLinearClassifier(images, labels)

    m = size(images, 2);

    % Add constant feature (x_0)
    features = sparse([ones(1,m); images]);
    A = features';

    N = size(features, 1);
    weights = zeros(10,N);
    
    % Train one-vs-all classifiers
    for digit=0:9
        digit
        b = double(labels==digit);
        weights(digit+1,:) = lsqr(A, b);
    end