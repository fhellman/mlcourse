function [labels, scores] = testLinearClassifier(weights, images)

m = size(images, 2);

% Add constant feature (x_0)
features = sparse([ones(1,m); images]);

% Compute scores for each digit and image
scores = weights*features;

% Pick the ones with highest score
[~, labelsp1] = max(scores);
labels = labelsp1' - 1;

