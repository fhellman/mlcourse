function [theta] = trainLogisticClassifier(images, labels)

    m = size(images, 2);

    % Add constant feature (x_0)
    X = sparse([ones(1,m); images]);

    n = size(X, 1);
    theta = zeros(10,n);
    
    % Train one-vs-all classifiers
    for digit=0:9
        digit
        y = double(labels==digit);
        y = y(:)';
        theta(digit+1,:) = newton(X, y, 3);
    end