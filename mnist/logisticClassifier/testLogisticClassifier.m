function [labels, scores] = testLogisticClassifier(theta, images)

m = size(images, 2);

% Add constant feature (x_0)
X = sparse([ones(1,m); images]);

sigmoidfunc = @(z) 1./(1+exp(-z));
hfunc = @(theta, X) sigmoidfunc(theta'*X);

% Compute scores for each digit and image
scores = zeros(10,m);
for digit=0:9
    scores(digit+1,:) = hfunc(theta(digit+1,:)', X);
end

% Pick the ones with highest score
[~, labelsp1] = max(scores);
labels = labelsp1' - 1;

