function [theta] = newton(X, y, maxIter)

n = size(X, 1);
m = size(X, 2);

iter = 0;
theta = zeros(n,1);

fprintf('Starting Newton iterations')
while iter < maxIter
    % Instead of solving 
    %   deltatheta = DF^-1 * F.
    % i.e.
    %   X*D*X'*deltatheta = -X*(y-h)'
    % we solve
    %   X*D^1/2 * D^1/2*X' * deltatheta = X*D^1/2 * -D^(-1/2)*(y-h)'
    % which can be interpreted as the lls-problem:
    %   min ||A * deltatheta - b ||
    % with A = D^1/2 * X'
    %  and b = D^(-1/2)*(y-h)'
    
    [Dhalf, Dmhalf, h, J] = computeDh(theta, X, y);
    ftheta = -1/m*X*(y-h)';
    
    A = 1/m * Dhalf * X';
    b = -1/m * Dmhalf * (y-h)';
    
    deltatheta = lsqr(A, b, [], 10);
    
    fprintf('      Iter %5d, initial ||f|| = %8.5e, J = %8.5e, ||deltatheta|| = %8.5e\n', iter, norm(ftheta), J, norm(deltatheta));
    theta = theta - deltatheta(:);
    
    iter = iter + 1;
end
[Dhalf, Dmhalf, h, J] = computeDh(theta, X, y);
ftheta = -1/m * X*(y-h)';
fprintf('Final iter %5d, ||f|| = %8.5e, J = %8.5e, last ||deltatheta|| = %8.5e\n', iter, norm(ftheta), J, norm(deltatheta));

function [Dhalf, Dmhalf, h, J] = computeDh(theta, X, y)

m = size(X,2);

sigmoidfunc = @(z) 1./(1+exp(-z));
hfunc = @(theta, X) sigmoidfunc(theta'*X);

% Compute f
h = hfunc(theta, X);
diagSqrt = sqrt([h.*(1-h)]');

Dhalf = spdiags(diagSqrt, 0, m, m);
Dmhalf = spdiags(1./diagSqrt, 0, m, m);

J = -1/m*sum(((1-y).*log(1-h) + y.*log(h)));