function errorRate = verifyLogisticClassifier()

% Train classifier
trainImages = loadMNISTImages('../train-images-idx3-ubyte');
trainLabels = loadMNISTLabels('../train-labels-idx1-ubyte');
theta = trainLogisticClassifier(trainImages, trainLabels);

% Test classifier
testImages = loadMNISTImages('../t10k-images-idx3-ubyte');
testLabelsPredicted = testLogisticClassifier(theta, testImages);
testLabelsCorrect = loadMNISTLabels('../t10k-labels-idx1-ubyte');

errorRate = sum(testLabelsCorrect ~= testLabelsPredicted)/length(testLabelsCorrect);

